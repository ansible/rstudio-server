# RStudio Server

This Ansible role installs the latest RStudio Server.
Tested on Ubuntu 18/20/22.

Since RStudio v1.3 it's supposedly possible to control all RStudio settings from
JSON config files. I have not yet had time to explore the possibilities.


## Quarto

> A stable release of Quarto is bundled with RStudio v2022.07.1 and later.
> Upgrading to new versions of RStudio in the future will also upgrade the bundled Quarto version.
> https://docs.posit.co/ide/user/ide/guide/documents/quarto-project.html

But the RStudio Server project may take some time before Quarto upgrade is bundled,
e.g., [Bundle and support Quarto 1.6 #15460](https://github.com/rstudio/rstudio/issues/15460).

```
$ tree -d -L 2 /usr/lib/rstudio-server/bin/quarto/
/usr/lib/rstudio-server/bin/quarto/
├── bin
│   ├── tools
│   └── vendor
└── share
    ├── capabilities
    ├── conf
    ├── create
    ├── deno_std
    ├── editor
    ├── env
    ├── extensions
    ├── filters
    ├── formats
    ├── js
    ├── julia
    ├── jupyter
    ├── language
    ├── library
    ├── lua-plugin
    ├── lua-types
    ├── man
    ├── pandoc
    ├── preview
    ├── projects
    ├── rmd
    ├── schema
    ├── scripts
    ├── types
    ├── use
    └── wasm

30 directories

$ ll /usr/lib/rstudio-server/bin/quarto/bin/
total 4.9M
drwxr-xr-x 4 root root 4.0K dec 20 00:52 .
drwxr-xr-x 4 root root 4.0K jul 29  2022 ..
drwxr-xr-x 3 root root 4.0K maj 29  2024 tools
drwxr-xr-x 4 root root 4.0K dec 20 00:52 vendor
-rwxr-xr-x 1 root root 6.9K aug 29 19:50 quarto
-rw-r--r-- 1 root root 4.9M aug 29 19:50 quarto.js
```

This installation of Quarto is available inside RStudio server without any further action.
But it is not visible to the rest of the system, unless we symlink it to somewhere
on `PATH` or add it to `PATH`.



## Links and notes

+ https://github.com/rstudio/rstudio
+ An Ansible playbook to setup R, Rstudio server and Shiny server on a Raspberry Pi.
  Actually compiles R from source, impressive!
  https://andresrcs.rbind.io/2021/01/13/raspberry_pi_server
  https://github.com/andresrcs/raspberry_pi_server
+ [Auth timeout setting](https://github.com/rstudio/rstudio/issues/5449)
+ http://www.danieldsjoberg.com/rstudio.prefs
+ https://rstudio.com/products/rstudio/release-notes
