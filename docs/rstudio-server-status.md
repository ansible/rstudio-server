We were leaving the configuration of rstudio-server's systemd unit file to the
rstudio-server installer, but maybe we should handle it ourselves going forward?
Or just sit tight and see what changes are introduced in the next rstudio-server
update?

```
root@luxor:/etc/rstudio
# cat /lib/systemd/system/rstudio-server.service
[Unit]
Description=RStudio Server
After=network-online.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/run/rstudio-server.pid
ExecStart=/usr/lib/rstudio-server/bin/rserver
ExecStop=/usr/bin/killall -TERM rserver
KillMode=none
Restart=on-failure

[Install]
WantedBy=multi-user.target

# rstudio-server status
● rstudio-server.service - RStudio Server
     Loaded: loaded (/lib/systemd/system/rstudio-server.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-09-14 15:51:22 CEST; 2 months 4 days ago
   Main PID: 3633673 (rserver)
      Tasks: 4 (limit: 38018)
     Memory: 120.6M
        CPU: 2h 27min 15.276s
     CGroup: /system.slice/rstudio-server.service
             └─3633673 /usr/lib/rstudio-server/bin/rserver

nov 09 11:26:13 luxor systemd[1]: /lib/systemd/system/rstudio-server.service:11: Unit configured to use KillMode=none. This is unsafe, as it disables systemd's process lifecycle management for the service. Please update your service to use a safer KillMode=, such as 'mixed' or 'control-group'. Support for KillMode=none is deprecated and will eventually be removed.
nov 17 03:48:55 luxor systemd[1]: /lib/systemd/system/rstudio-server.service:11: Unit configured to use KillMode=none. This is unsafe, as it disables systemd's process lifecycle management for the service. Please update your service to use a safer KillMode=, such as 'mixed' or 'control-group'. Support for KillMode=none is deprecated and will eventually be removed.
nov 17 03:48:59 luxor systemd[1]: /lib/systemd/system/rstudio-server.service:11: Unit configured to use KillMode=none. This is unsafe, as it disables systemd's process lifecycle management for the service. Please update your service to use a safer KillMode=, such as 'mixed' or 'control-group'. Support for KillMode=none is deprecated and will eventually be removed.
nov 17 03:49:00 luxor systemd[1]: /lib/systemd/system/rstudio-server.service:11: Unit configured to use KillMode=none. This is unsafe, as it disables systemd's process lifecycle management for the service. Please update your service to use a safer KillMode=, such as 'mixed' or 'control-group'. Support for KillMode=none is deprecated and will eventually be removed.
nov 18 03:37:08 luxor rsession-taha[579689]: WARNING Invalid values found in /etc/rstudio/rstudio-prefs.json: /global_theme; LOGGED FROM: rstudio::core::Error rstudio::session::prefs::PrefLayer::loadPrefsFromFile(const rstudio::core::FilePath&, const rstudio::core::FilePath&) src/cpp/session/prefs/PrefLayer.cpp:164
nov 18 03:37:29 luxor rsession-taha[580041]: WARNING Invalid values found in /etc/rstudio/rstudio-prefs.json: /global_theme; LOGGED FROM: rstudio::core::Error rstudio::session::prefs::PrefLayer::loadPrefsFromFile(const rstudio::core::FilePath&, const rstudio::core::FilePath&) src/cpp/session/prefs/PrefLayer.cpp:164
nov 18 03:38:37 luxor rsession-taha[580041]: ERROR system error 2 (No such file or directory) [path: , dcf-file: ]; OCCURRED AT rstudio::core::Error rstudio::core::FilePath::openForRead(std::shared_ptr<std::basic_istream<char> >&) const src/cpp/shared_core/FilePath.cpp:1462; LOGGED FROM: void rstudio::session::projects::{anonymous}::syncProjectFileChanges() src/cpp/session/projects/SessionProjects.cpp:511
nov 18 04:36:52 luxor rsession-taha[580041]: ERROR system error 2 (No such file or directory) [path: , dcf-file: ]; OCCURRED AT rstudio::core::Error rstudio::core::FilePath::openForRead(std::shared_ptr<std::basic_istream<char> >&) const src/cpp/shared_core/FilePath.cpp:1462; LOGGED FROM: void rstudio::session::projects::{anonymous}::syncProjectFileChanges() src/cpp/session/projects/SessionProjects.cpp:511
nov 18 04:46:59 luxor rsession-taha[580041]: ERROR system error 2 (No such file or directory) [path: , dcf-file: ]; OCCURRED AT rstudio::core::Error rstudio::core::FilePath::openForRead(std::shared_ptr<std::basic_istream<char> >&) const src/cpp/shared_core/FilePath.cpp:1462; LOGGED FROM: void rstudio::session::projects::{anonymous}::syncProjectFileChanges() src/cpp/session/projects/SessionProjects.cpp:511
nov 18 04:47:33 luxor rsession-taha[580041]: ERROR system error 2 (No such file or directory) [path: , dcf-file: ]; OCCURRED AT rstudio::core::Error rstudio::core::FilePath::openForRead(std::shared_ptr<std::basic_istream<char> >&) const src/cpp/shared_core/FilePath.cpp:1462; LOGGED FROM: void rstudio::session::projects::{anonymous}::syncProjectFileChanges() src/cpp/session/projects/SessionProjects.cpp:511
```
